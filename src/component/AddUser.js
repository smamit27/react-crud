import React, {useState } from 'react'
import USER_DETAIL from './UserDetail';

function AddUser(props) {


    const [UserProfile,setUserProfile] = useState(USER_DETAIL);
    const userProfileHandler = (event) =>{
        const {name,value} = event.target;
        setUserProfile({...UserProfile,[name]: value});
    }

return (

    <form onSubmit={(event)=>{event.preventDefault();
          if(!UserProfile.name || !UserProfile.username) return
          props.addUserDetail(UserProfile)
          setUserProfile(USER_DETAIL);
    }}>
    <div className="mb-3">
    <label htmlFor="name" className="form-label">Name</label>
    <input type="text" 
            className="form-control" 
            id="name" 
            name="name" 
            placeholder="Enter your name"
            value={UserProfile.name}
            onChange={userProfileHandler}
           />
    </div>
    <div className="mb-3">
    <label htmlFor="username" 
            className="form-label">User Name </label>
    <input type="text" 
            className="form-control" 
            id="username" 
            name="username" 
            placeholder="Enter your username"
            value={UserProfile.username}
            onChange={userProfileHandler} 
             />
            
    </div>
    <div className="mb-3">
        <button type="submit" className="btn btn-primary">
            Add User Profile
        </button>
    </div>
    </form>
    )
}

export default AddUser
