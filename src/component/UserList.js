import React, { Fragment } from 'react'

function UserList(props) {
    return (
        <Fragment>
        <table className="table">
            <thead>
                <tr>
                <th className="col">#</th>
                <th className="col">Name</th>
                <th className="col">User Name</th>
                <th className="col">Edit</th>
                <th className="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.user.length > 0 ? (
                  props.user.map((userItem,index)=>
                    (
                        <tr key={index}>
                        <th >{userItem.id}</th>
                        <td>{userItem.name}</td>
                        <td>{userItem.username}</td>
                        <td onClick={()=> props.editUser(userItem)}>Edit</td>
                        <td onClick={()=>props.deleteUser(userItem.id)}>Delete</td>
        
                    </tr>
                    )))
                    : (
                        <tr>
                            <td colSpan={3}>No users</td>
                        </tr>
                    )}
       
            </tbody>
        </table>
        </Fragment>
    )
}

export default UserList
