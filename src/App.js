import React,{useState} from 'react';
import './App.css';
import UserList from './component/UserList';
import AddUser from './component/AddUser';
import USER_LIST_ITEM from './component/UserInfo';
import USER_DETAIL from './component/UserDetail'
import EditUser from './component/EditUser';

function App() {
      
  //When user click on Add User button
  const [user,setUser] = useState(USER_LIST_ITEM); 
  const addUserDetail=(userdata)=>{
    userdata.id = user.length + 1;
    setUser([...user,userdata]);
  }
  
  //When user click on Edit button and that is part of user list 
  const [currentState,setCurrentState] = useState(USER_DETAIL);
  //Set the Flag
  const flag = false;
  const [EditFlag,setEditFlag] = useState(flag);
  const editUser = (userItem) =>{
    setEditFlag(true);
    setCurrentState({
      id: userItem.id,
      name: userItem.name,
      username: userItem.username
    });

  }
  //Update User
  const updateUser = (id,updatedUser) =>{
    setEditFlag(false);
    setUser(user.map((user)=>(user.id === id ? updatedUser: user)))

  }
  //Delete User data
  const deleteUser =(id)=>{
    const deleteProfile = user.filter((ids)=>ids.id !== id);
    setUser(deleteProfile);
  }
  return (
    <div className="App">
      <div className="container">
      <h1> React Crud App1</h1>
          <div className="row">
            <div className="col-sm ">
              {EditFlag ? <EditUser setEditFlag={setEditFlag} 
                                    currentState={currentState}
                                    updateUser={updateUser}/> :  
                          <AddUser addUserDetail={addUserDetail} />
              }
            </div>
            <div className="col-sm">
              <UserList editUser={editUser} 
                        deleteUser={deleteUser} 
                        user={user}/>
            </div>
          </div>
      </div>
    </div>
  );
}

export default App;
